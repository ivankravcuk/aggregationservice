package com.cgi.aggregationservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketWithPersonsDto {

    PersonDto personAssigned;
    PersonDto personCreator;
    TicketDto ticket;
}
