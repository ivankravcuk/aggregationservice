package com.cgi.aggregationservice.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonDto {

    @JsonAlias(value = "person_id")
    private Long personId;
    private String name;
    private String email;
    private AddressDto address;
}
