package com.cgi.aggregationservice.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TicketDto implements Serializable {
    @JsonAlias(value = "ticket_id")
    private Long ticketId;
    private Long idPersonAssigned;
    private Long idPersonCreator;
    private String name;
    private String email;
    //I don't understand why this one doesn't need @JsonAlias like the others,
    // I guess something to do with my terrible DateTime conversion
    private LocalDateTime creationDatetime;
}
