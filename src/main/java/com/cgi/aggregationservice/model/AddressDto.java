package com.cgi.aggregationservice.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressDto {
    @JsonAlias(value = "address_id")
    private Long addressId;
    private String city;
    private String country;
    private String street;
    @JsonAlias(value = "post_code")
    private String postCode;
}
