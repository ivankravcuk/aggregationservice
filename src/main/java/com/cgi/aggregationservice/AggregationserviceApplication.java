package com.cgi.aggregationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AggregationserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AggregationserviceApplication.class, args);
    }

}
