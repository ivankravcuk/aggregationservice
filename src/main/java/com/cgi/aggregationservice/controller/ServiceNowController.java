package com.cgi.aggregationservice.controller;

import com.cgi.aggregationservice.model.PersonDto;
import com.cgi.aggregationservice.model.TicketDto;
import com.cgi.aggregationservice.model.TicketWithPersonsDto;
import com.cgi.aggregationservice.service.ServiceNowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/v1/servicenow")
public class ServiceNowController {

    private final ServiceNowService serviceNowService;

    @Autowired
    public ServiceNowController(ServiceNowService serviceNowService) {
        this.serviceNowService = serviceNowService;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<TicketDto> findTicketById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(serviceNowService.findByIdTicket(id));
    }

    @GetMapping
    public ResponseEntity<List<TicketDto>> findAllTickets() {
        return ResponseEntity.ok(serviceNowService.findAll());
    }

    @GetMapping(path = "/fullticket/{id}")
    public ResponseEntity<TicketWithPersonsDto> FindPersonByTicketId(@PathVariable("id") Long id) {
        return ResponseEntity.ok(serviceNowService.FindPersonByTicketId(id));
    }
}
