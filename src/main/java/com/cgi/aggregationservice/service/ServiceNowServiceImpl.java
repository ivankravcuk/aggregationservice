package com.cgi.aggregationservice.service;

import com.cgi.aggregationservice.configuration.RestPageImpl;
import com.cgi.aggregationservice.model.PersonDto;
import com.cgi.aggregationservice.model.TicketDto;
import com.cgi.aggregationservice.model.TicketWithPersonsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class ServiceNowServiceImpl implements ServiceNowService {

    private final String serviceNowURL = "http://localhost:8083/tickets";
    private final String userManagementURL = "http://localhost:8084/persons";
    private final RestTemplate restTemplate;

    @Autowired
    public ServiceNowServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public TicketDto findByIdTicket(Long id) {
        return restTemplate.getForObject(serviceNowURL + "/" + id, TicketDto.class);
    }

    @Override
    public List<TicketDto> findAll() {

        ParameterizedTypeReference<RestPageImpl<TicketDto>> responseType = new ParameterizedTypeReference<RestPageImpl<TicketDto>>() {
        };

        ResponseEntity<RestPageImpl<TicketDto>> result = restTemplate.exchange(serviceNowURL, HttpMethod.GET,
                null, responseType);

        return Objects.requireNonNull(result.getBody()).getContent();
    }

    @Override
    public TicketWithPersonsDto FindPersonByTicketId(Long idPersonAssigned) {

        TicketDto ticketDto = restTemplate.getForObject(serviceNowURL + "/" + idPersonAssigned, TicketDto.class);
        PersonDto personAssigned = restTemplate.getForObject(userManagementURL + "/" + idPersonAssigned, PersonDto.class);
        PersonDto personCreator = restTemplate.getForObject(userManagementURL + "/" +
                ticketDto.getIdPersonCreator(), PersonDto.class);

        return new TicketWithPersonsDto(personAssigned, personCreator, ticketDto);
    }
}
