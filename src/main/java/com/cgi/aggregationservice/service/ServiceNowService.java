package com.cgi.aggregationservice.service;

import com.cgi.aggregationservice.model.PersonDto;
import com.cgi.aggregationservice.model.TicketDto;
import com.cgi.aggregationservice.model.TicketWithPersonsDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ServiceNowService {

    TicketDto findByIdTicket(Long id);

    List<TicketDto> findAll();

    TicketWithPersonsDto FindPersonByTicketId(Long id);
}
